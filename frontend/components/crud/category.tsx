import React, { useEffect, useState } from 'react';

import { getCookie, isAuth } from '../../actions/auth';
import { create, list, read, remove } from '../../actions/category';

/// <reference path="../../../shared/shared.d.ts"/>
import { ICategory } from "src/models/category"

interface IState {
  name: string,
  categories: ICategory[],
  error: string,
  success: boolean,
  removed: boolean,
  reload: boolean
}

const Category = () => {
  const [values, setValues] = useState<IState>({
    name: "",
    categories: [],
    error: "",
    success: false,
    removed: false,
    reload: false
  })

  const { name, error, success, categories, removed, reload } = values
  const token = getCookie("token")

  useEffect(() => {
    console.log("loading...")
    loadCategories()
  }, [reload])

  const loadCategories = () => {
    list().then(data => {
      if (data.error) {
        console.log(data.error)
      } else {
        setValues({...values, categories: data})
      }
    })
  }

  const showCategories = () => {
    return categories.map((c, i) => {
      return (
        <button
          key={i}
          className="btn btn-outline-primary mr-1 ml-1 mt-3"
          title="Double click to delete"
          onDoubleClick={() => deleteConfirm(c.slug)}
        >
          {c.name}
        </button>
      )
    })
  }

  const deleteConfirm = slug => {
    let answer = window.confirm("Are you sure you want to delete this category?")
    if (answer) {
      deleteCategory(slug)
    }
  }

  const deleteCategory = slug => {
    remove(slug, token).then(data => {
      if (data.error) {
        console.log(data.error)
      } else {
        setValues({ ...values, error: "", success: false, name: "", removed: true, reload: !reload })
      }
    })
  }

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault()
    create({ name }, token).then(data => {
      if (data.error) {
        setValues({...values, error: data.error, success: false })
      } else {
        setValues({...values, error: "", success: true, name: "", reload: !reload })
      }
    })
  }

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValues({...values, name: e.target.value, error: "", success: false, removed: false })
  }

  const showSuccess = () => {
    if (success) {
      return <p className="text-success">Category is created</p>
    }
  }

  const showError = () => {
    if (error) {
      return <p className="text-danger">{error}</p>
    }
  }

  const showRemoved = () => {
    if (removed) {
      return <p className="text-danger">Category is removed</p>
    }
  }

  const mouseMoveHandler = e => {
    setValues({ ...values, error: "", success: false, removed: false })
  }

  const newCategoryForm = () => (
    <form onSubmit={handleSubmit}>
      <div className="form-group">
        <label className="text-muted">Add new category name:</label>
        <input type="text" className="form-control" onChange={handleChange} value={name} required/>
      </div>
      <div>
        <button type="submit" className="btn btn-primary">Create</button>
      </div>
    </form>
  )

  return <React.Fragment>
    {showSuccess()}
    {showError()}
    {showRemoved()}
    <div onMouseMove={mouseMoveHandler}>
      {newCategoryForm()}
      {showCategories()}
    </div>
  </React.Fragment>
}

export default Category
