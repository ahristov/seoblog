import dynamic from 'next/dynamic';
import Link from 'next/link';
import Router, { withRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { getCookie, isAuth } from '../../actions/auth'
import { list as listCategories } from '../../actions/category'
import { list as listTags } from '../../actions/tag'
import { create as createBlog } from '../../actions/blog'

const ReactQuill = dynamic(() => import('react-quill'), {
    ssr: false
  });

const LS_BLOG_NAME = "seoblog_blog_draft"

const CreateBlog = ({ router }) => {
  const blogFromLocalStorage = () => {
    if (typeof window !== "undefined") {
      const storedItem = localStorage.getItem(LS_BLOG_NAME)
      if (storedItem) {
        return JSON.parse(storedItem)
      }
    }

    return ""
  }

  const blogToLocalStorage = (e) => {
    if (typeof window !== "undefined") {
      localStorage.setItem(LS_BLOG_NAME, JSON.stringify(e))
    }
  }

  const [categories, setCategories] = useState([])
  const [tags, setTags] = useState([])
  const [checkedCategories, setCheckedCategories] = useState([])
  const [checkedTags, setCheckedTags] = useState([])
  const [body, setBody] = useState(blogFromLocalStorage())
  const [values, setValues] = useState({
    error: "",
    sizeError: "",
    success: false,
    formData: null as FormData,
    title: "",
    hidePublishButton: false
  })

  const { error, sizeError, success, formData, title, hidePublishButton } = values

  useEffect(() => {
    setValues({...values, formData: new FormData()})
    initCategories()
    initTags()
  }, [router])

  const initCategories = () => {
    listCategories().then(data => {
      if (data.error) {
        setValues({...values, error: data.error})
      } else {
        setCategories(data)
      }
    })
  }

  const initTags = () => {
    listTags().then(data => {
      if (data.error) {
        setValues({...values, error: data.error})
      } else {
        setTags(data)
      }
    })
  }

  const publishBlog = (e) => {
    e.preventDefault()
    console.log("ready to publish blog")
  }

  const handleChange = name => e => {
    console.log(name, e.target.value)

    const value = name === "photo" ? e.target.files[0] : e.target.value
    formData.set(name, value)
    setValues({...values, [name]: value, formData, error: ""})
  }

  const handleBodyChange = e => {
    setBody(e)
    formData.set("body", e)
    blogToLocalStorage(e)
  }

  const handleToggleCategory = (id) => () => {
    setValues({...values, error: ""})
    const checkedIdx = checkedCategories.indexOf(id)
    const all = [...checkedCategories]

    if (checkedIdx < 0) {
      all.push(id)
    } else {
      all.splice(checkedIdx, 1)
    }

    setCheckedCategories(all)
    formData.set("categories", all.join(","))
  }

  const handleToggleTag = (id) => () => {
    setValues({...values, error: ""})
    const checkedIdx = checkedTags.indexOf(id)
    const all = [...checkedTags]

    if (checkedIdx < 0) {
      all.push(id)
    } else {
      all.splice(checkedIdx, 1)
    }

    setCheckedTags(all)
    formData.set("tags", all.join(","))
  }

  const showCategories = () => {
    return (
      categories && categories.map((c, i) => (
        <li key={i} className="list-unstyled">
          <input onChange={handleToggleCategory(c._id)} type="checkbox" className="mr-2"/>
          <label className="form-check-label">{c.name}</label>
        </li>
      ))
    )
  }

  const showTags = () => {
    return (
      tags && tags.map((t, i) => (
        <li key={i} className="list-unstyled">
          <input onChange={handleToggleTag(t._id)} type="checkbox" className="mr-2"/>
          <label className="form-check-label">{t.name}</label>
        </li>
      ))
    )
  }

  const createBlogForm = () => {
    return (
      <form onSubmit={publishBlog}>
        <div className="form-group">
          <label className="text-muted">Title</label>
          <input type="text" className="form-control" value={title} onChange={handleChange("title")}/>
        </div>

        <div className="form-group">
          <ReactQuill
            value={body}
            modules={CreateBlog.modules}
            formats={CreateBlog.formats}
            placeholder="Write something amazing"
            onChange={handleBodyChange}
          />
        </div>

        <div>
          <button type="submit" className="btn btn-primary">
            Publish
          </button>
        </div>
      </form>
    )
  }

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-md-8">
          {createBlogForm()}
          <hr/>
          {JSON.stringify(title)}
          <hr/>
          {JSON.stringify(body)}
          <hr/>
          {JSON.stringify(categories)}
          <hr/>
          {JSON.stringify(tags)}
        </div>
        <div className="col-md-4">
          <div>
            <div className="form-group pb-2">
              <h5>Featured image</h5>
              <hr/>
              <div>
                <small className="text-muted">Max size: 1mb</small>
              </div>
              <div>
                <label className="btn btn-outline-info">
                  Upload featured image
                  <input onChange={handleChange("photo")} type="file" accept="image/*" hidden/>
                </label>
                </div>
            </div>
          </div>
          <div>
            <h5>Categories</h5>
            <hr/>
            <ul style={{ maxHeight: '200px', overflowY: 'scroll' }}>
              {showCategories()}
            </ul>
          </div>
          <div>
            <h5>Tags</h5>
            <hr/>
            <ul style={{ maxHeight: '200px', overflowY: 'scroll' }}>
              {showTags()}
            </ul>
          </div>
        </div>
      </div>
    </div>
  )
}

CreateBlog.modules = {
  toolbar: [
    [ { header: "1" }, { header: "2" }, { header: [3,4,5,6] }, { font: [] } ],
    [ { size: [] } ],
    [ "bold", "italic", "underline", "strike", "blockquote"] ,
    [ { list: "ordered" }, { list: "bullet" } ],
    [ "link", "image", "video" ],
    [ "clean" ],
    [ "code-block" ]
  ]
}

CreateBlog.formats = [
  "header",
  "font",
  "size",
  "bold",
  "italic",
  "underline",
  "strike",
  "blockquote",
  "list",
  "bullet",
  "link",
  "image",
  "video",
  "code-block"
]

export default withRouter(CreateBlog)