import Router from 'next/router'
import React, { useEffect } from 'react'
import { isAdmin, isAuth } from '../../actions/auth'
import * as Paths from "../../const/paths"

const AdminAccess = (props:React.PropsWithChildren<{}>) => {
  useEffect(() => {
    if (!isAuth()) {
      Router.push(Paths.PATH_SIGIN)
    } else if (!isAdmin()) {
      Router.push(Paths.PATH_HOME)
    }
  }, [])

  if (!isAdmin()) {
    return null
  }

  return (
    <React.Fragment>{props.children}</React.Fragment>
  )
}

export default AdminAccess