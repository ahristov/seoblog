import Router from 'next/router'
import React, { useState, useEffect } from 'react'

import { signup, isAuth } from '../../actions/auth'
import * as Paths from "../../const/paths"

const SignupComponent = () => {
  const [values, setValues] = useState({
    name: "testuser",
    email: "testuser@test.com",
    password: "test1234",
    error: "",
    loading: false,
    message: "",
    showForm: true
  })

  const { name, email, password, error, loading, message, showForm } = values

  useEffect(() => {
    if (isAuth()) {
      Router.push(Paths.PATH_HOME)
    }
  }, [])

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault()
    // console.table(name, email, password, error, loading, message, showForm)
    setValues({ ...values, loading: true, error: "", })
    const user = { name, email, password }
    signup(user)
      .then(data => {
        if (data.error) {
          setValues({
            ...values,
            error: data.error,
            loading: false
          })
        } else {
          setValues({
            ...values,
            name: "",
            email: "",
            password: "",
            error: "",
            loading: false,
            message: data.message,
            showForm: false
          })
        }
      })
  }

  const handleChange = (name: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
    setValues({ ...values, error: "", [name]: e.target.value })
  }

  const showLoading = () => (loading ? <div className="alert alert-info">Loading...</div> : "")
  const showError = () => (error ? <div className="alert alert-danger">{error}</div> : "")
  const showMessage = () => (message ? <div className="alert alert-info">{message}</div> : "")

  const signupForm = () => {
    if (isAuth()) {
      return null
    }

    return (
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <input
            value={name}
            onChange={(handleChange("name"))}
            type="text"
            className="form-control"
            placeholder="Type your name"
          />
        </div>
        <div className="form-group">
          <input
            value={email}
            onChange={handleChange("email")}
            type="email"
            className="form-control"
            placeholder="Type your email"
          />
        </div>
        <div className="form-group">
          <input
            value={password}
            onChange={handleChange("password")}
            type="password"
            className="form-control"
            placeholder="Type your password"
          />
        </div>
        <div>
          <button className="btn btn-primary">Signup</button>
        </div>
      </form>
    )
  }

  return (
    <React.Fragment>
      {showError()}
      {showLoading()}
      {showMessage()}
      {showForm && signupForm()}
    </React.Fragment>
  )
}

export default SignupComponent