import Router from 'next/router';
import React, { useEffect, useState } from 'react';

import { authenticate, isAdmin, isAuth, signin } from '../../actions/auth';
import * as Paths from "../../const/paths"

const SigninComponent = () => {
  const [values, setValues] = useState({
    email: "testuser@test.com",
    password: "test1234",
    error: "",
    loading: false,
    message: "",
    showForm: true
  })

  const { email, password, error, loading, message, showForm } = values

  useEffect(() => {
    if (isAuth()) {
      Router.push(Paths.PATH_HOME)
    }
  }, [])

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault()
    // console.table(name, email, password, error, loading, message, showForm)
    setValues({...values, loading: true, error: "", })
    const user = { email, password }
    signin(user)
      .then(data => {
        if (data.error) {
          setValues({...values,
            error: data.error,
            loading: false
          })
        } else {
          // save user token to cookie
          // save user info to localStorage
          // authenticate user
          authenticate(data, () => {
            if (isAdmin())
            {
              Router.push(Paths.PATH_ADMIN)
            }
            else
            {
              Router.push(Paths.PATH_USER)
            }
          })
        }
      })
  }

  const handleChange = (name:string) => (e: React.ChangeEvent<HTMLInputElement>) => {
    setValues({...values, error: "", [name]: e.target.value })
  }

  const showLoading = () =>(loading ? <div className="alert alert-info">Loading...</div> : "")
  const showError = () =>(error ? <div className="alert alert-danger">{error}</div> : "")
  const showMessage = () =>(message ? <div className="alert alert-info">{message}</div> : "")

  const signinForm = () => {
    if (isAuth()) {
      return null
    }

    return (
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <input
            value={email}
            onChange={handleChange("email")}
            type="email"
            className="form-control"
            placeholder="Type your email"
          />
        </div>
        <div className="form-group">
          <input
            value={password}
            onChange={handleChange("password")}
            type="password"
            className="form-control"
            placeholder="Type your password"
          />
        </div>
        <div>
          <button className="btn btn-primary">Signin</button>
        </div>
      </form>
    )
  }

  return (
    <React.Fragment>
      {showError()}
      {showLoading()}
      {showForm && signinForm()}
      {showMessage()}
    </React.Fragment>
  )
}

export default SigninComponent