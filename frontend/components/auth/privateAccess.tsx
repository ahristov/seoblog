import Router from 'next/router'
import React, { useEffect } from 'react'
import { isAuth } from '../../actions/auth'
import * as Paths from "../../const/paths"

const PrivateAccess = (props:React.PropsWithChildren<{}>) => {
  useEffect(() => {
    if (!isAuth()) {
      Router.push(Paths.PATH_SIGIN)
    }
  }, [])

  if (!isAuth()) {
    return null
  }

  return (
    <React.Fragment>{props.children}</React.Fragment>
  )
}

export default PrivateAccess