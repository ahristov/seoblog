import Link from 'next/link'
import Router from 'next/router'
import React, { useState } from 'react'
import { Collapse, Nav, Navbar, NavbarToggler, NavItem, NavLink } from 'reactstrap'
import NProgress from "nprogress"

import { isAdmin, isAuth, signout } from '../actions/auth'
import { APP_NAME } from '../config'
import * as Paths from "../const/paths"

Router.events.on('routeChangeStart', () => NProgress.start())
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

const Header = () => {
  const [isOpen, setIsOpen] = useState(false)
  const toggle = () => {
    setIsOpen(!isOpen)
  }

  return (
    <div>
      <Navbar color="light" light expand="md">
        <Link href={Paths.PATH_HOME}>
          <NavLink style={{ cursor: 'pointer' }} className="font-weight-bold">{APP_NAME}</NavLink>
        </Link>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            {!isAuth() && (
              <React.Fragment>
                <NavItem>
                  <Link href={Paths.PATH_SIGIN}>
                    <NavLink style={{ cursor: 'pointer' }}>Signin</NavLink>
                  </Link>
                </NavItem>
                <NavItem>
                  <Link href={Paths.PATH_SIGNUP}>
                    <NavLink style={{ cursor: 'pointer' }}>Signup</NavLink>
                  </Link>
                </NavItem>
              </React.Fragment>
            )}
            {isAuth() && (
              <React.Fragment>
                <NavItem>
                  <Link href={isAdmin() ? Paths.PATH_ADMIN : Paths.PATH_USER}>
                      <NavLink style={{ cursor: 'pointer' }}>{`${isAuth().name}`}'s Dashboard</NavLink>
                  </Link>
                </NavItem>
                <NavItem>
                  <NavLink style={{ cursor: 'pointer' }} onClick={() => signout(() => Router.replace(Paths.PATH_HOME))} >Signout</NavLink>
                </NavItem>
              </React.Fragment>
            )}

          </Nav>
        </Collapse>
      </Navbar>
    </div>
  )
}

export default Header