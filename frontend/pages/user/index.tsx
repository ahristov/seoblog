import Layout from '../../components/layout'
import PrivateAccess from '../../components/auth/privateAccess'

const UserIndex = () => {
  return (
    <Layout>
      <PrivateAccess>
        <h2>User Dashboard</h2>
      </PrivateAccess>
    </Layout>
  )
}

export default UserIndex