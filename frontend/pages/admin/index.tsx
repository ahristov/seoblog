import Link from "next/link"

import AdminAccess from "../../components/auth/adminAccess"
import Layout from "../../components/layout"
import { PATH_ADMIN_CRUD_BLOG, PATH_ADMIN_CRUD_CATEGORY_TAG } from "../../const/paths"

const AdminIndex = () => {
  return (
    <Layout>
      <AdminAccess>
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12 pt-4 pb-4">
              <h2>Admin Dashboard</h2>
            </div>
            <div className="col-md-4">
              <ul className="list-group">
                <li className="list-group-item">
                  <Link href={PATH_ADMIN_CRUD_CATEGORY_TAG}>
                    <a>Create Category</a>
                  </Link>
                </li>
                <li className="list-group-item">
                  <Link href={PATH_ADMIN_CRUD_CATEGORY_TAG}>
                    <a>Create Tag</a>
                  </Link>
                </li>
                <li className="list-group-item">
                  <Link href={PATH_ADMIN_CRUD_BLOG}>
                    <a>Create Blog</a>
                  </Link>
                </li>
              </ul>
            </div>
            <div className="col-md-8">
              right
            </div>
          </div>
        </div>
      </AdminAccess>
    </Layout>
  )
}

export default AdminIndex