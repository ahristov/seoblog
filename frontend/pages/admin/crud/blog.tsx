import AdminAccess from "../../../components/auth/adminAccess"
import Layout from "../../../components/layout"
import CreateBlog from "../../../components/crud/blogCreate"
import Link from 'next/link'

const Blog = () => {
  return (
    <Layout>
      <AdminAccess>
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12 pt-4 pb-4">
              <h2>Create blog</h2>
            </div>
            <div className="col-md-12">
              <CreateBlog />
            </div>
          </div>
        </div>
      </AdminAccess>
    </Layout>
  )
}

export default Blog