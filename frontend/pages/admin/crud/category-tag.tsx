import AdminAccess from "../../../components/auth/adminAccess"
import Category from "../../../components/crud/category"
import Tag from "../../../components/crud/tag"
import Layout from "../../../components/layout"

const CatetogyTag = () => {
  return (
    <Layout>
      <AdminAccess>
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12 pt-4 pb-4">
              <h2>Manage Categories and Tags</h2>
            </div>
            <div className="col-md-6">
              <Category/>
            </div>
            <div className="col-md-6">
              <Tag/>
            </div>
          </div>
        </div>
      </AdminAccess>
    </Layout>
  )
}

export default CatetogyTag