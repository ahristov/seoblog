import Layout from '../components/layout'

const Home = () => {
  return (
    <Layout>
      <h1>Hello world!</h1>
    </Layout>
  )
}

export default Home