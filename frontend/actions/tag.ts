import { API } from '../config'
import restApi from './restApi'

/// <reference path="../../../shared/shared.d.ts"/>
import { ITag } from "src/models/tag"
import { IError } from "src/models/error"

type TagResp = ITag & IError
type TagsResp = ITag[] & IError

const create = (tag: ITag, token: string) =>
  restApi<TagResp>("POST", `${API}/tags/tag`, tag, token)

const list = () =>
  restApi<TagsResp>("GET", `${API}/tags`)

const read = (slug:string) =>
  restApi<TagResp>("GET", `${API}/tags/tag/${slug}`)

const remove = (slug:string, token: string) =>
  restApi<IError>("DELETE", `${API}/tags/tag/${slug}`, null, token)

export {
  create,
  list,
  read,
  remove
}