import { API } from '../config'
import restApi from './restApi'

/// <reference path="../../../shared/shared.d.ts"/>
import { IBlog } from "src/models/blog"
import { IError } from "src/models/error"

type BlogResp = IBlog & IError
type BlogsResp = IBlog[] & IError

const create = (blog: IBlog, token: string) =>
  restApi<BlogResp>("POST", `${API}/blogs/blog`, blog, token, "")

export {
  create
}
