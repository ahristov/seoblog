import fetch from 'isomorphic-fetch'

const headers = (
  token?: string,
  contentType: string="application/json"
) => {

  if (contentType)
  {
    return token
      ? {
        "Accept": "application/json",
        "Content-Type": `${contentType}`,
        "Authorization": `Bearer ${token}`
      } : {
        "Accept": "application/json",
        "Content-Type": "application/json",
      }
  } else {
    return token
      ? {
        "Accept": "application/json",
        "Authorization": `Bearer ${token}`
      } : {
        "Accept": "application/json",
      }
  }
}

const restApi = <T>(
  method: "GET" | "POST" | "PUT" | "DELETE",
  url: string,
  data?: any,
  token?: string,
  contentType: string="application/json"): Promise<T> => {
    return fetch(url, {
      method,
      headers: headers(token, contentType),
      body: data ? JSON.stringify(data) : undefined
    })
    .then(response => {
      if (!response.ok) {
        console.log("response", response.statusText)
        return { error: response.statusText }
      }
      return response.json()
    })
    .catch(err => {console.log(err)})
}


export default restApi