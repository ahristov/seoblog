import { API } from '../config'
import restApi from './restApi'

/// <reference path="../../../shared/shared.d.ts"/>
import { ICategory } from "src/models/category"
import { IError } from "src/models/error"

type CategoryResp = ICategory & IError
type CategoriesResp = ICategory[] & IError

const create = (category: ICategory, token: string) =>
  restApi<CategoryResp>("POST", `${API}/categories/category`, category, token)

const list = () =>
  restApi<CategoriesResp>("GET", `${API}/categories`)

const read = (slug:string) =>
  restApi<CategoryResp>("GET", `${API}/categories/category/${slug}`)

const remove = (slug:string, token: string) =>
  restApi<IError>("DELETE", `${API}/categories/category/${slug}`, null, token)

export {
  create,
  list,
  read,
  remove
}