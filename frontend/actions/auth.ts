import fetch from 'isomorphic-fetch'
import { API } from '../config'
import cookie from 'js-cookie'

/// <reference path="../../../shared/shared.d.ts"/>
import { IUser } from "src/models/user"

// signup

const signup = (user) => {
  return fetch(`${API}/auth/signup`, {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(user)
  })
  .then(response => {
    return response.json()
  })
  .catch(err => console.log(err))
}

//signin

const signin = (user) => {
  return fetch(`${API}/auth/signin`, {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(user)
  })
  .then(response => {
    return response.json()
  })
  .catch(err => console.log(err))
}

// signout

const signout = (next) => {
  removeCookie("token")
  removeLocalStorage("user")
  next()

  return fetch(`${API}/auth/signout`, {
    method: "GET"
  })
  .then(response => {
    console.log("signout success")
  })
  .catch(err => console.log(err))
}

// cookie

const setCookie = (key, value) => {
  if (process.browser) {
    cookie.set(key, value, {
      expires: 1
    })
  }
}

const removeCookie = (key) => {
  if (process.browser) {
    cookie.remove(key, {
      expires: 1
    })
  }
}

const getCookie = (key) => {
  if (process.browser) {
    return cookie.get(key)
  }
}

// localStorage

const setLocalStorage = (key, value) => {
  if (process.browser) {
    localStorage.setItem(key, JSON.stringify(value))
  }
}

const removeLocalStorage = (key) => {
  if (process.browser) {
    localStorage.removeItem(key)
  }
}

const getLocalStorage = (key) => {
  if (process.browser) {
    return localStorage.getItem(key)
  }
}

// authenticate user by pass data to  cookie and localStorage

const authenticate = (data, next) => {
  setCookie("token", data.token)
  setLocalStorage("user", data.user)
  next()
}

const isAuth = (): IUser => {
  if (process.browser) {
    const cookieChecked = getCookie("token")
    if (cookieChecked) {
      const storedData = getLocalStorage("user")
      if (storedData) {
        return JSON.parse(storedData)
      } else {
        return null
      }
    }
  }
}

const isAdmin = ():Boolean => {
  const user = isAuth()
  return user && user.role === 1
}

export {
  signup,
  signin,
  signout,
  setCookie,
  removeCookie,
  getCookie,
  setLocalStorage,
  removeLocalStorage,
  getLocalStorage,
  authenticate,
  isAuth,
  isAdmin
}