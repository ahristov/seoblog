export interface IBlog {
  name: string,
  slug?: string,
  body: {},
  except?: string,
  metaTitle?: string,
  metaDesc?: string,
  photo?: any,
  categories: [],
  tags: [],
  postedBy: any
}
