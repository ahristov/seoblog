export interface IUser {
  username: string,
  name: string,
  email: string,
  profile: string,
  hashed_password: string,
  about: string,
  role: number,
  photo?: any,
}
