declare module "src/models/blog" {
    export interface IBlog {
        name: string;
        slug?: string;
        body: {};
        except?: string;
        metaTitle?: string;
        metaDesc?: string;
        photo?: any;
        categories: [];
        tags: [];
        postedBy: any;
    }
}
declare module "src/models/category" {
    export interface ICategory {
        name: string;
        slug?: string;
    }
}
declare module "src/models/error" {
    export interface IError {
        error?: string;
    }
}
declare module "src/models/tag" {
    export interface ITag {
        name: string;
        slug?: string;
    }
}
declare module "src/models/user" {
    export interface IUser {
        username: string;
        name: string;
        email: string;
        profile: string;
        hashed_password: string;
        about: string;
        role: number;
        photo?: any;
    }
}
declare module "src/utils/justlog" {
    export const justlog: () => void;
}
//# sourceMappingURL=shared.d.ts.map