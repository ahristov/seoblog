# SEO Blog

This project contains code from studying [React Node FullStack - Multi User Blogging Platform with SEO](https://www.packtpub.com/web-development/react-node-fullstack-multi-user-blogging-platform-with-seo-video) course.

## 🏠 [Homepage](https://bitbucket.org/ahristov/seoblog)

## Run

Run shared:

```bash
cd shared
yarn
yarn run dev
```

Run backend:

```bash
cd backend
yarn
yarn run dev
```

Run frontend:

```bash
cd frontend
yarn
yarn run dev
```

You want to add front-end configuration file `frontend/next.config.js`. Example:

```javascript
module.exports  = {
  publicRuntimeConfig: {
    APP_NAME: 'SEOBLOG',
    API_DEVELOPMENT: 'http://localhost:8000/api',
    API_PRODUCTION: 'https://seoblog/api',
    PRODUCTION: false
  }
}
```

Default values are set in `frontend/config.ts`.

## Author

👤 **Atanas Hristov**

### Links

[React Node FullStack - Multi User Blogging Platform with SEO](https://www.packtpub.com/web-development/react-node-fullstack-multi-user-blogging-platform-with-seo-video)  
