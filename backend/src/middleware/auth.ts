import dotenv from "dotenv"
import { NextFunction, Request, Response } from "express"
import expressJwt from "express-jwt"

import User, { IUser } from "../models/user"

// load env variables
dotenv.config()

const secret = process.env.JWT_SECRET

const requireSignin = expressJwt({ secret })

type RequestWithUser = Request & { user: { _id: ""} , profile: IUser }

const authUserData = (req: RequestWithUser, res: Response, next: NextFunction) => {
  const authUserId = req.user._id
  User.findById({ _id: authUserId }).exec((err, user: IUser) => {
    if (err || !user) {
      return res.status(400).json({
        error: "User not found"
      })
    }
    req.profile = user
    next()
  })
}

const adminUserOnly = (req: RequestWithUser, res: Response, next: NextFunction) => {
  const adminUserId = req.user._id
  User.findById({ _id: adminUserId }).exec((err, user: IUser) => {
    if (err || !user) {
      return res.status(400).json({
        error: "User not found"
      })
    }

    if (user.role !== 1) {
      return res.status(400).json({
        error: "Admin resource. Access denied"
      })
    }

    req.profile = user
    next()
  })
}

export {
  RequestWithUser,
  requireSignin,
  authUserData,
  adminUserOnly as adminUserOnly
}
