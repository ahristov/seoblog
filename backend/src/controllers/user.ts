import { Response } from "express"
import { RequestWithUser } from "../middleware/auth"

const profile = (req: RequestWithUser, res: Response) => {
  req.profile.hashed_password = undefined
  return res.json(req.profile)
}

export {
  profile
}
