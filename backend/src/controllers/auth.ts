import { Request, Response } from "express"
import jwt from "jsonwebtoken"
import shortid from "shortid"
import User, { UserType } from "../models/user"

const signup = (req: Request, res: Response) => {
  const { name, email, password } = req.body

  User.findOne({ email }).exec((err, user) => {
    if (user) {
      return res.status(400).json({
        error: "Email is taken."
      })
    }

    const username = shortid.generate()
    const profile = `${process.env.CLIENT_URL}/profile/${username}`
    const newUser = new User({ name, email, password, username, profile })
    newUser.save((errSave, success) => {
      if (errSave) {
        return res.status(400).json({
          error: errSave
        })
      }
      res.json({
        message: "Signup success! Please signin."
      })
    })
  })
}

const oneDay = "1d"
const oneDayMs = 1 * 24 * 60 * 60 * 1000
const cookieName = "token"

const signin = (req: Request, res: Response) => {
  const { email, password } = req.body

  // check user exists
  User.findOne({ email }).exec((err, user: UserType) => {
    if (err || !user) {
      return res.status(400).json({
        error: "User does not exist."
      })
    }

    // authenticate
    if (!user.authenticate(password)) {
      return res.status(400).json({
        error: "Email and password do not match."
      })
    }

    // generate a token and send to client
    const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET, { expiresIn: oneDay })
    res.cookie(cookieName, token, { maxAge: oneDayMs })

    const { _id, username, name, role } = user

    return res.json({
      token,
      user: { _id, username, name, email, role }
    })
  })
}

const signout = (req: Request, res: Response) => {
  res.clearCookie(cookieName)
  res.json({
    message: "Signout success."
  })
}

export {
  signup,
  signin,
  signout
}
