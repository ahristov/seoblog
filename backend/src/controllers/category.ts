import { Request, Response } from "express"
import slugify from "slug"
import dbErrorHandler from "../helpers/dbErrorHandler"
import Category from "../models/category"

const create = (req: Request, res: Response) => {
  const { name } = req.body
  const slug = slugify(name).toLowerCase()
  const category = new Category({ name, slug })
  category.save((err, data) => {
    if (err) {
      if (err.code === 11000) {
        res.statusMessage = "Category already exists"
        return res.status(422).json({
          error: res.statusMessage
        })
      } else {
        return res.status(400).json({
          error: dbErrorHandler(err)
        })
      }
    }
    res.json(data)
  })
}

const list = (req: Request, res: Response) => {
  Category.find({}).exec((err, data) => {
    if (err) {
      return res.status(400).json({
        error: dbErrorHandler(err)
      })
    }
    res.json(data)
  })
}

const read = (req: Request, res: Response) => {
  const slug = req.params.slug.toLowerCase()
  Category.findOne({ slug }).exec((err, data) => {
    if (err) {
      return res.status(400).json({
        error: dbErrorHandler(err)
      })
    }
    res.json(data) // TODO: return also the blogs associated with it
  })
}

const remove = (req: Request, res: Response) => {
  const slug = req.params.slug.toLowerCase()
  Category.findOneAndRemove({ slug }).exec((err, data) => {
    if (err) {
      return res.status(400).json({
        error: dbErrorHandler(err)
      })
    }
    res.json({
      message: "Category deleted successfully"
    })
  })
}

export {
  create,
  list,
  read,
  remove
}
