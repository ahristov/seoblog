import { Request, Response } from "express"
// import formidable from "formidable"
// import fs from "fs"
import _ from "lodash"
import slugify from "slug"
import stripHtml from "string-strip-html"

import dbErrorHandler from "../helpers/dbErrorHandler"
import { smartTrim } from "../helpers/string"
import { RequestWithUser } from "../middleware/auth"
import Blog, { EXCERPT_MAX_LEN } from "../models/blog"

const META_DESC_LEN = 160
const EXCERPT_LEN = 320

const create = (req: RequestWithUser, res: Response) => {
  const { title, body, categories, tags } = req.body
  const slug = slugify(title as string).toLowerCase()
  const postedBy = req.user._id

  const metaTitle = `${title} | ${process.env.APP_NAME}`
  const metaDesc = smartTrim(stripHtml(body), META_DESC_LEN, " ", "")

  const excerptLen = EXCERPT_LEN <= EXCERPT_MAX_LEN ? EXCERPT_LEN : EXCERPT_MAX_LEN
  const excerpt = smartTrim(stripHtml(body), excerptLen, " ", " ...")

  const arrayOfCategories = categories && categories.split(",")
  const arrayOfTags = tags && tags.split(",")

  /* tslint:disable: object-literal-sort-keys */
  const blog = {
    title,
    slug,
    body,
    excerpt,
    metaTitle,
    metaDesc,
    postedBy,
    photo: null as any
  }

  if (req.file) {
    blog.photo = {
      data: req.file.buffer,  // fs.readFileSync(req.file.path),
      contentType: req.file.mimetype
    }
  }

  new Blog({...blog}).save((err, data) => {
    if (err) {
      if (err.code === 11000) {
        res.statusMessage = "Blog with this title already exists"
        return res.status(422).json({
          err: res.statusMessage
        })
      } else {
        return res.status(400).json({
          error: dbErrorHandler(err)
        })
      }
    }
    Blog.findByIdAndUpdate(data._id, {$push: {categories: arrayOfCategories}}, {new: true}).exec((err1, data1) => {
      if (err1) {
        return res.status(400).json({
          error: dbErrorHandler(err1)
        })
      }
      Blog.findByIdAndUpdate(data._id, {$push: {tags: arrayOfTags}}, {new: true}).exec((err2, data2) => {
        if (err2) {
          console.log(err2)
          return res.status(400).json({
            error: dbErrorHandler(err2)
          })
        }
        res.json(data2)
      })
    })
  })
}

export {
  create
}
