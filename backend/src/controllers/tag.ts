import { Request, Response } from "express"
import slugify from "slug"
import dbErrorHandler from "../helpers/dbErrorHandler"
import Tag from "../models/tag"

const create = (req: Request, res: Response) => {
  const { name } = req.body
  const slug = slugify(name).toLowerCase()
  const tag = new Tag({ name, slug })
  tag.save((err, data) => {
    if (err) {
      if (err.code === 11000) {
        res.statusMessage = "Tag already exists"
        return res.status(422).json({
          error: res.statusMessage
        })
      } else {
        return res.status(400).json({
          error: dbErrorHandler(err)
        })
      }
    }
    res.json(data)
  })
}

const list = (req: Request, res: Response) => {
  Tag.find({}).exec((err, data) => {
    if (err) {
      return res.status(400).json({
        error: dbErrorHandler(err)
      })
    }
    res.json(data)
  })
}

const read = (req: Request, res: Response) => {
  const slug = req.params.slug.toLowerCase()
  Tag.findOne({ slug }).exec((err, data) => {
    if (err) {
      return res.status(400).json({
        error: dbErrorHandler(err)
      })
    }
    res.json(data) // TODO: return also the blogs associated with it
  })
}

const remove = (req: Request, res: Response) => {
  const slug = req.params.slug.toLowerCase()
  Tag.findOneAndRemove({ slug }).exec((err, data) => {
    if (err) {
      return res.status(400).json({
        error: dbErrorHandler(err)
      })
    }
    res.json({
      message: "Tag deleted successfully"
    })
  })
}

export {
  create,
  list,
  read,
  remove
}
