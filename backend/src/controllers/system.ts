import { Request, Response } from "express"

const timeNow = (req: Request, res: Response) => {
  res.json({ time: Date().toString() })
}

export {
  timeNow
}
