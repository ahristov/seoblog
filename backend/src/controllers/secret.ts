import { Response } from "express"
import { RequestWithUser } from "../middleware/auth"

// secret place to test authentication
const secretPlace = (req: RequestWithUser, res: Response) => {
  console.log("req", req)
  res.json({
    message: req.user
  })
}

export {
  secretPlace
}
