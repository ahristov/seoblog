import crypto from "crypto"
import mongoose, { Document } from "mongoose"

/// <reference path="../../../shared/shared.d.ts"/>
import { IUser } from "src/models/user"

/* tslint:disable: object-literal-sort-keys */

type UserType = IUser & Document & {
  authenticate: (password: string) => boolean
}

const userSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      trim: true,
      required: true,
      maxlength: 32,
      unique: true,
      index: true,
      lowercase: true
    },
    name: {
      type: String,
      trim: true,
      required: true,
      maxlength: 32
    },
    email: {
      type: String,
      trim: true,
      required: true,
      unique: true,
      lowercase: true
    },
    profile: {
      type: String,
      required: true
    },
    hashed_password: {
      type: String,
      required: true
    },
    salt: {
      type: String
    },
    about: {
      type: String
    },
    role: {
      type: Number,
      default: 0
    },
    photo: {
      data: Buffer,
      contentType: String
    },
    resetPasswordLink: {
      data: String,
      default: ""
    }
  },
  { timestamps: true }
)

userSchema.virtual("password")
  .set(function(
    this: {
      _password: string,
      salt: string,
      makeSalt: () => string,
      encryptPassword: (password: string) => string,
      hashed_password: string
    },
    password: string
  ) {
    // create a temporary variable called _password
    this._password = password

    // generate salt
    this.salt = this.makeSalt()

    // encrypt password
    this.hashed_password = this.encryptPassword(password)
  })
  .get(function(this: {_password: string}) {
    return this._password
  })

userSchema.methods = {
  authenticate(
    this: {
      encryptPassword: (password: string) => string,
      hashed_password: string
    },
    password: string) {
      return this.encryptPassword(password) === this.hashed_password
  },
  encryptPassword(this: { salt: string }, password: string) {
    if (!password) {
      return ""
    }
    try {
      return crypto.createHmac("sha1", this.salt)
        .update(password)
        .digest("hex")
    } catch (err) {
      return ""
    }
  },
  makeSalt() {
    return Math.round(new Date().valueOf() * Math.random()) + ""
  }
}

export default mongoose.model("User", userSchema)

export {
  IUser,
  UserType
}
