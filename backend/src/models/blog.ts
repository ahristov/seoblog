import mongoose, { Document, mongo } from "mongoose"

/// <reference path="../../../shared/shared.d.ts"/>
import { IBlog } from "src/models/blog"

const Schema = mongoose.Schema
const ObjectId = mongoose.Schema.Types.ObjectId

/* tslint:disable: object-literal-sort-keys */

type BlogType = IBlog & Document & {
}

const TITLE_MIN_LEN = 3
const TITLE_MAX_LEN = 160
const BODY_MIN_LEN = 100
const BODY_MAX_LEN = 200000
const EXCERPT_MAX_LEN = 1000

const blogSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      trim: true,
      minlength: TITLE_MIN_LEN,
      maxlength: TITLE_MAX_LEN,
      required: true
    },
    slug: {
      type: String,
      unique: true,
      index: true
    },
    body: {
      type: {},
      minlength: BODY_MIN_LEN,
      maxlength: BODY_MAX_LEN
    },
    excerpt: {
      type: String,
      maxlength: EXCERPT_MAX_LEN
    },
    metaTitle: {
      type: String
    },
    metaDesc: {
      type: String
    },
    photo: {
      data: Buffer,
      contentType: String
    },
    categories: [{type: ObjectId, ref: "Category", required: true}],
    tags: [{type: ObjectId, ref: "Tag", required: true}],
    postedBy: {type: ObjectId, ref: "User", required: true}
  },
  { timestamps: true }
)

export default mongoose.model("Blog", blogSchema)

export {
  IBlog,
  BlogType,
  TITLE_MIN_LEN,
  TITLE_MAX_LEN,
  BODY_MIN_LEN,
  BODY_MAX_LEN,
  EXCERPT_MAX_LEN
}
