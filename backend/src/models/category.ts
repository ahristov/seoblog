import mongoose, { Document } from "mongoose"

/// <reference path="../../../shared/shared.d.ts"/>
import { ICategory } from "src/models/category"

/* tslint:disable: object-literal-sort-keys */

type CategoryType = ICategory & Document & {
}

const categorySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
      required: true,
      maxLength: 32
    },
    slug: {
      type: String,
      unique: true,
      index: true
    },
  },
  { timestamps: true }
)

export default mongoose.model("Category", categorySchema)

export {
  ICategory,
  CategoryType
}
