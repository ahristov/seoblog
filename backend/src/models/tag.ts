import mongoose, { Document } from "mongoose"

/// <reference path="../../../shared/shared.d.ts"/>
import { ITag } from "src/models/tag"

/* tslint:disable: object-literal-sort-keys */

type TagType = ITag & Document & {
}

const tagSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
      required: true,
      maxLength: 32
    },
    slug: {
      type: String,
      unique: true,
      index: true
    },
  },
  { timestamps: true }
)

export default mongoose.model("Tag", tagSchema)

export {
  ITag,
  TagType
}
