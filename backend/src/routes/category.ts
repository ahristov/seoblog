import express from "express"
import { create, list, read, remove } from "../controllers/category"
import { adminUserOnly, authUserData, requireSignin } from "../middleware/auth"
import { runValidation } from "../validators"
import { categoryCreateValidator } from "../validators/category"

const router = express.Router()

router.post("/category", requireSignin, authUserData, adminUserOnly, categoryCreateValidator, runValidation, create)
router.delete("/category/:slug", requireSignin, authUserData, adminUserOnly, remove)
router.get("/", list)
router.get("/category/:slug", read)

export default router
