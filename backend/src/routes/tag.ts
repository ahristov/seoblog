import express from "express"
import { create, list, read, remove } from "../controllers/tag"
import { adminUserOnly, authUserData, requireSignin } from "../middleware/auth"
import { runValidation } from "../validators"
import { tagCreateValidator } from "../validators/tag"

const router = express.Router()

router.post("/tag", requireSignin, authUserData, adminUserOnly, tagCreateValidator, runValidation, create)
router.delete("/tag/:slug", requireSignin, authUserData, adminUserOnly, remove)
router.get("/", list)
router.get("/tag/:slug", read)

export default router
