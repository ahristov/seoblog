import express from "express"
import multer from "multer"
import { create } from "../controllers/blog"
import { adminUserOnly, authUserData, requireSignin } from "../middleware/auth"
import { runValidation } from "../validators"
import { blogCreateValidator } from "../validators/blog"
import photoValidation from "../validators/photo"

const router = express.Router()
const upload = multer()

router.post("/blog", requireSignin, authUserData, adminUserOnly,
  upload.single("photo"), blogCreateValidator, runValidation, photoValidation, create)

export default router
