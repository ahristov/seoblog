import express from "express"
import { timeNow } from "../controllers/system"

const router = express.Router()

router.get("/system/time", timeNow)

export default router
