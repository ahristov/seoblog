import express from "express"
import { secretPlace } from "../controllers/secret"

const router = express.Router()

router.get("/place", secretPlace)

export default router
