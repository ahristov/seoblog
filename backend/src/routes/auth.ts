import express from "express"
import { signin, signout, signup } from "../controllers/auth"
import { runValidation } from "../validators"
import { userSigninValidator, userSignupValidator } from "../validators/auth"

const router = express.Router()

router.post("/signup", userSignupValidator, runValidation, signup)
router.post("/signin", userSigninValidator, runValidation, signin)
router.get("/signout", signout)

export default router
