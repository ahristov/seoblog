import bodyParser from "body-parser"
import cookieParser from "cookie-parser"
import cors from "cors"
import dotenv from "dotenv"
import express from "express"
import mongoose from "mongoose"
import morgan from "morgan"
import { authUserData, requireSignin } from "./middleware/auth"
import authRoutes from "./routes/auth"
import blogRoutes from "./routes/blog"
import categoryRoutes from "./routes/category"
import secretRoutes from "./routes/secret"
import systemRoutes from "./routes/system"
import tagRoutes from "./routes/tag"
import userRoutes from "./routes/user"

// load env variables
dotenv.config()

// app
const app = express()

// db
mongoose
  .connect(process.env.MONGODB, {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => console.log("DB connected."))
  .catch((reason) => console.log("DB connect error", reason))

// middleware
app.use(morgan("dev"))
app.use(bodyParser.json())
app.use(cookieParser())

// cors
if (process.env.NODE_ENV === "development") {
  app.use(cors({ origin: `${process.env.CLIENT_URL}` }))
}

const secret = process.env.JWT_SECRET

// routes middleware
app.use("/api/auth", authRoutes)
app.use("/api/secret", requireSignin, secretRoutes)
app.use("/api/user", requireSignin, authUserData, userRoutes)
app.use("/api/categories", categoryRoutes)
app.use("/api/tags", tagRoutes)
app.use("/api/blogs", blogRoutes)
app.use("/api/system", systemRoutes)

// port
const port = process.env.PORT || 8000

app.listen(port, () => {
  console.log(`Backend started at http://localhost:${port}/api`)
})
