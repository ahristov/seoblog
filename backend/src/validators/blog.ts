import { check } from "express-validator"
import {
  BODY_MAX_LEN,
  BODY_MIN_LEN,
  TITLE_MAX_LEN,
  TITLE_MIN_LEN
} from "../models/blog"

const blogCreateValidator = [
  // body("title", "Blog title is required").isLength({min: 3}),
  // body("body", "Blog body is required").isLength({min: 100})
  // body("title").custom((value) => {
  //   // This is your email.
  //    console.log("title", value)
  // })
  check("title")
    .not()
    .isEmpty()
    .withMessage("Blog title is required"),
  check("title")
    .isLength({min: TITLE_MIN_LEN})
    .withMessage("Blog title too short"),
  check("title")
    .isLength({max: TITLE_MAX_LEN})
    .withMessage("Blog title too long"),
  check("body")
    .not()
    .isEmpty()
    .withMessage("Blog body is required"),
  check("body")
    .isLength({min: BODY_MIN_LEN})
    .withMessage("Blog body too short"),
  check("body")
    .isLength({max: BODY_MAX_LEN})
    .withMessage("Blog body too long"),
  check("categories")
    .not()
    .isEmpty()
    .withMessage("At least one category is required"),
  check("tags")
    .not()
    .isEmpty()
    .withMessage("At least one tag is required")
/*
  check("categories")
    .custom((item) => {
      try {
        const arr = JSON.parse(item)
        return arr.length > 0
      } catch (err) {
        return false
      }
    })
    .withMessage("At least one category is required"),
  check("tags")
  .custom((item) => {
    try {
      const arr = JSON.parse(item)
      return arr.length > 0
    } catch (err) {
      return false
    }
  })
  .withMessage("At least one tag is required")
*/

]

export {
  blogCreateValidator
}
