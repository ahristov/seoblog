import { NextFunction, Request, Response } from "express"
import path from "path"

const photoValidation = (req: Request, res: Response, next: NextFunction) => {
  const { file } = req

  if (file) {
    if (file.size > 10000000) {
      return res.status(400).json({
        error: "Image should be less than 1mb in size"
      })
    }

    const ext = path.extname(file.originalname).toLowerCase()

    if (ext !== ".png" && ext !== ".jpg" && ext !== ".gif" && ext !== ".jpeg") {
      return res.status(400).json({
        error: "Only images files are allowed"
      })
    }

    if (!file.mimetype.startsWith("image")) {
      return res.status(400).json({
        error: "Only images can be uploaded"
      })
    }
  }

  next()
}

export default photoValidation
