import { check } from "express-validator"

const tagCreateValidator = [
  check("name")
    .not()
    .isEmpty()
    .withMessage("Tag name is required")
]

export {
  tagCreateValidator
}
