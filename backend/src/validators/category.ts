import { check } from "express-validator"

const categoryCreateValidator = [
  check("name")
    .not()
    .isEmpty()
    .withMessage("Category name is required")
]

export {
  categoryCreateValidator
}
