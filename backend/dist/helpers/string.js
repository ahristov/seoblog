"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const smartTrim = (str, length, delim, appendix) => {
    if (str.length <= length) {
        return str;
    }
    let trimmedStr = str.substr(0, length + delim.length);
    const lastDelimIndex = trimmedStr.lastIndexOf(delim);
    if (lastDelimIndex >= 0) {
        trimmedStr = trimmedStr.substr(0, lastDelimIndex);
    }
    return trimmedStr
        ? trimmedStr += appendix
        : trimmedStr;
};
exports.smartTrim = smartTrim;
//# sourceMappingURL=string.js.map