"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const photoValidation = (req, res, next) => {
    const { file } = req;
    if (file) {
        if (file.size > 10000000) {
            return res.status(400).json({
                error: "Image should be less than 1mb in size"
            });
        }
        const ext = path_1.default.extname(file.originalname).toLowerCase();
        if (ext !== ".png" && ext !== ".jpg" && ext !== ".gif" && ext !== ".jpeg") {
            return res.status(400).json({
                error: "Only images files are allowed"
            });
        }
        if (!file.mimetype.startsWith("image")) {
            return res.status(400).json({
                error: "Only images can be uploaded"
            });
        }
    }
    next();
};
exports.default = photoValidation;
//# sourceMappingURL=photo.js.map