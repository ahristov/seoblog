"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_validator_1 = require("express-validator");
const categoryCreateValidator = [
    express_validator_1.check("name")
        .not()
        .isEmpty()
        .withMessage("Category name is required")
];
exports.categoryCreateValidator = categoryCreateValidator;
//# sourceMappingURL=category.js.map