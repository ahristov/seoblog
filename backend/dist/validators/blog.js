"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_validator_1 = require("express-validator");
const blog_1 = require("../models/blog");
const blogCreateValidator = [
    // body("title", "Blog title is required").isLength({min: 3}),
    // body("body", "Blog body is required").isLength({min: 100})
    // body("title").custom((value) => {
    //   // This is your email.
    //    console.log("title", value)
    // })
    express_validator_1.check("title")
        .not()
        .isEmpty()
        .withMessage("Blog title is required"),
    express_validator_1.check("title")
        .isLength({ min: blog_1.TITLE_MIN_LEN })
        .withMessage("Blog title too short"),
    express_validator_1.check("title")
        .isLength({ max: blog_1.TITLE_MAX_LEN })
        .withMessage("Blog title too long"),
    express_validator_1.check("body")
        .not()
        .isEmpty()
        .withMessage("Blog body is required"),
    express_validator_1.check("body")
        .isLength({ min: blog_1.BODY_MIN_LEN })
        .withMessage("Blog body too short"),
    express_validator_1.check("body")
        .isLength({ max: blog_1.BODY_MAX_LEN })
        .withMessage("Blog body too long"),
    express_validator_1.check("categories")
        .not()
        .isEmpty()
        .withMessage("At least one category is required"),
    express_validator_1.check("tags")
        .not()
        .isEmpty()
        .withMessage("At least one tag is required")
    /*
      check("categories")
        .custom((item) => {
          try {
            const arr = JSON.parse(item)
            return arr.length > 0
          } catch (err) {
            return false
          }
        })
        .withMessage("At least one category is required"),
      check("tags")
      .custom((item) => {
        try {
          const arr = JSON.parse(item)
          return arr.length > 0
        } catch (err) {
          return false
        }
      })
      .withMessage("At least one tag is required")
    */
];
exports.blogCreateValidator = blogCreateValidator;
//# sourceMappingURL=blog.js.map