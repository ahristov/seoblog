"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_validator_1 = require("express-validator");
const userSignupValidator = [
    express_validator_1.check("name")
        .not()
        .isEmpty()
        .withMessage("Name is required"),
    express_validator_1.check("email")
        .isEmail()
        .withMessage("Must be a valid email address"),
    express_validator_1.check("password")
        .isLength({ min: 6 })
        .withMessage("Password must be at least 6 characters long")
];
exports.userSignupValidator = userSignupValidator;
const userSigninValidator = [
    express_validator_1.check("email")
        .isEmail()
        .withMessage("Must be a valid email address"),
    express_validator_1.check("password")
        .isLength({ min: 6 })
        .withMessage("Password must be at least 6 characters long")
];
exports.userSigninValidator = userSigninValidator;
//# sourceMappingURL=auth.js.map