"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_validator_1 = require("express-validator");
const tagCreateValidator = [
    express_validator_1.check("name")
        .not()
        .isEmpty()
        .withMessage("Tag name is required")
];
exports.tagCreateValidator = tagCreateValidator;
//# sourceMappingURL=tag.js.map