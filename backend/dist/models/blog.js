"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const Schema = mongoose_1.default.Schema;
const ObjectId = mongoose_1.default.Schema.Types.ObjectId;
const TITLE_MIN_LEN = 3;
exports.TITLE_MIN_LEN = TITLE_MIN_LEN;
const TITLE_MAX_LEN = 160;
exports.TITLE_MAX_LEN = TITLE_MAX_LEN;
const BODY_MIN_LEN = 100;
exports.BODY_MIN_LEN = BODY_MIN_LEN;
const BODY_MAX_LEN = 200000;
exports.BODY_MAX_LEN = BODY_MAX_LEN;
const EXCERPT_MAX_LEN = 1000;
exports.EXCERPT_MAX_LEN = EXCERPT_MAX_LEN;
const blogSchema = new mongoose_1.default.Schema({
    title: {
        type: String,
        trim: true,
        minlength: TITLE_MIN_LEN,
        maxlength: TITLE_MAX_LEN,
        required: true
    },
    slug: {
        type: String,
        unique: true,
        index: true
    },
    body: {
        type: {},
        minlength: BODY_MIN_LEN,
        maxlength: BODY_MAX_LEN
    },
    excerpt: {
        type: String,
        maxlength: EXCERPT_MAX_LEN
    },
    metaTitle: {
        type: String
    },
    metaDesc: {
        type: String
    },
    photo: {
        data: Buffer,
        contentType: String
    },
    categories: [{ type: ObjectId, ref: "Category", required: true }],
    tags: [{ type: ObjectId, ref: "Tag", required: true }],
    postedBy: { type: ObjectId, ref: "User", required: true }
}, { timestamps: true });
exports.default = mongoose_1.default.model("Blog", blogSchema);
//# sourceMappingURL=blog.js.map