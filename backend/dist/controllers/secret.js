"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// secret place to test authentication
const secretPlace = (req, res) => {
    console.log("req", req);
    res.json({
        message: req.user
    });
};
exports.secretPlace = secretPlace;
//# sourceMappingURL=secret.js.map