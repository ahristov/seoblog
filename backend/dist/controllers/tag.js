"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const slug_1 = __importDefault(require("slug"));
const dbErrorHandler_1 = __importDefault(require("../helpers/dbErrorHandler"));
const tag_1 = __importDefault(require("../models/tag"));
const create = (req, res) => {
    const { name } = req.body;
    const slug = slug_1.default(name).toLowerCase();
    const tag = new tag_1.default({ name, slug });
    tag.save((err, data) => {
        if (err) {
            if (err.code === 11000) {
                res.statusMessage = "Tag already exists";
                return res.status(422).json({
                    error: res.statusMessage
                });
            }
            else {
                return res.status(400).json({
                    error: dbErrorHandler_1.default(err)
                });
            }
        }
        res.json(data);
    });
};
exports.create = create;
const list = (req, res) => {
    tag_1.default.find({}).exec((err, data) => {
        if (err) {
            return res.status(400).json({
                error: dbErrorHandler_1.default(err)
            });
        }
        res.json(data);
    });
};
exports.list = list;
const read = (req, res) => {
    const slug = req.params.slug.toLowerCase();
    tag_1.default.findOne({ slug }).exec((err, data) => {
        if (err) {
            return res.status(400).json({
                error: dbErrorHandler_1.default(err)
            });
        }
        res.json(data); // TODO: return also the blogs associated with it
    });
};
exports.read = read;
const remove = (req, res) => {
    const slug = req.params.slug.toLowerCase();
    tag_1.default.findOneAndRemove({ slug }).exec((err, data) => {
        if (err) {
            return res.status(400).json({
                error: dbErrorHandler_1.default(err)
            });
        }
        res.json({
            message: "Tag deleted successfully"
        });
    });
};
exports.remove = remove;
//# sourceMappingURL=tag.js.map