"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const shortid_1 = __importDefault(require("shortid"));
const user_1 = __importDefault(require("../models/user"));
const signup = (req, res) => {
    const { name, email, password } = req.body;
    user_1.default.findOne({ email }).exec((err, user) => {
        if (user) {
            return res.status(400).json({
                error: "Email is taken."
            });
        }
        const username = shortid_1.default.generate();
        const profile = `${process.env.CLIENT_URL}/profile/${username}`;
        const newUser = new user_1.default({ name, email, password, username, profile });
        newUser.save((errSave, success) => {
            if (errSave) {
                return res.status(400).json({
                    error: errSave
                });
            }
            res.json({
                message: "Signup success! Please signin."
            });
        });
    });
};
exports.signup = signup;
const oneDay = "1d";
const oneDayMs = 1 * 24 * 60 * 60 * 1000;
const cookieName = "token";
const signin = (req, res) => {
    const { email, password } = req.body;
    // check user exists
    user_1.default.findOne({ email }).exec((err, user) => {
        if (err || !user) {
            return res.status(400).json({
                error: "User does not exist."
            });
        }
        // authenticate
        if (!user.authenticate(password)) {
            return res.status(400).json({
                error: "Email and password do not match."
            });
        }
        // generate a token and send to client
        const token = jsonwebtoken_1.default.sign({ _id: user._id }, process.env.JWT_SECRET, { expiresIn: oneDay });
        res.cookie(cookieName, token, { maxAge: oneDayMs });
        const { _id, username, name, role } = user;
        return res.json({
            token,
            user: { _id, username, name, email, role }
        });
    });
};
exports.signin = signin;
const signout = (req, res) => {
    res.clearCookie(cookieName);
    res.json({
        message: "Signout success."
    });
};
exports.signout = signout;
//# sourceMappingURL=auth.js.map