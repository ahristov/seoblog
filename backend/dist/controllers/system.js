"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const timeNow = (req, res) => {
    res.json({ time: Date().toString() });
};
exports.timeNow = timeNow;
//# sourceMappingURL=system.js.map