"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const slug_1 = __importDefault(require("slug"));
const string_strip_html_1 = __importDefault(require("string-strip-html"));
const dbErrorHandler_1 = __importDefault(require("../helpers/dbErrorHandler"));
const string_1 = require("../helpers/string");
const blog_1 = __importStar(require("../models/blog"));
const META_DESC_LEN = 160;
const EXCERPT_LEN = 320;
const create = (req, res) => {
    const { title, body, categories, tags } = req.body;
    const slug = slug_1.default(title).toLowerCase();
    const postedBy = req.user._id;
    const metaTitle = `${title} | ${process.env.APP_NAME}`;
    const metaDesc = string_1.smartTrim(string_strip_html_1.default(body), META_DESC_LEN, " ", "");
    const excerptLen = EXCERPT_LEN <= blog_1.EXCERPT_MAX_LEN ? EXCERPT_LEN : blog_1.EXCERPT_MAX_LEN;
    const excerpt = string_1.smartTrim(string_strip_html_1.default(body), excerptLen, " ", " ...");
    const arrayOfCategories = categories && categories.split(",");
    const arrayOfTags = tags && tags.split(",");
    /* tslint:disable: object-literal-sort-keys */
    const blog = {
        title,
        slug,
        body,
        excerpt,
        metaTitle,
        metaDesc,
        postedBy,
        photo: null
    };
    if (req.file) {
        blog.photo = {
            data: req.file.buffer,
            contentType: req.file.mimetype
        };
    }
    new blog_1.default(Object.assign({}, blog)).save((err, data) => {
        if (err) {
            if (err.code === 11000) {
                res.statusMessage = "Blog with this title already exists";
                return res.status(422).json({
                    err: res.statusMessage
                });
            }
            else {
                return res.status(400).json({
                    error: dbErrorHandler_1.default(err)
                });
            }
        }
        blog_1.default.findByIdAndUpdate(data._id, { $push: { categories: arrayOfCategories } }, { new: true }).exec((err1, data1) => {
            if (err1) {
                return res.status(400).json({
                    error: dbErrorHandler_1.default(err1)
                });
            }
            blog_1.default.findByIdAndUpdate(data._id, { $push: { tags: arrayOfTags } }, { new: true }).exec((err2, data2) => {
                if (err2) {
                    console.log(err2);
                    return res.status(400).json({
                        error: dbErrorHandler_1.default(err2)
                    });
                }
                res.json(data2);
            });
        });
    });
};
exports.create = create;
//# sourceMappingURL=blog.js.map