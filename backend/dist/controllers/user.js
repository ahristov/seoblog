"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const profile = (req, res) => {
    req.profile.hashed_password = undefined;
    return res.json(req.profile);
};
exports.profile = profile;
//# sourceMappingURL=user.js.map