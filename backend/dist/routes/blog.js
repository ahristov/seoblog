"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const multer_1 = __importDefault(require("multer"));
const blog_1 = require("../controllers/blog");
const auth_1 = require("../middleware/auth");
const validators_1 = require("../validators");
const blog_2 = require("../validators/blog");
const photo_1 = __importDefault(require("../validators/photo"));
const router = express_1.default.Router();
const upload = multer_1.default();
router.post("/blog", auth_1.requireSignin, auth_1.authUserData, auth_1.adminUserOnly, upload.single("photo"), blog_2.blogCreateValidator, validators_1.runValidation, photo_1.default, blog_1.create);
exports.default = router;
//# sourceMappingURL=blog.js.map