"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const category_1 = require("../controllers/category");
const auth_1 = require("../middleware/auth");
const validators_1 = require("../validators");
const category_2 = require("../validators/category");
const router = express_1.default.Router();
router.post("/category", auth_1.requireSignin, auth_1.authUserData, auth_1.adminUserOnly, category_2.categoryCreateValidator, validators_1.runValidation, category_1.create);
router.delete("/category/:slug", auth_1.requireSignin, auth_1.authUserData, auth_1.adminUserOnly, category_1.remove);
router.get("/", category_1.list);
router.get("/category/:slug", category_1.read);
exports.default = router;
//# sourceMappingURL=category.js.map