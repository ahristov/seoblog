"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const secret_1 = require("../controllers/secret");
const router = express_1.default.Router();
router.get("/place", secret_1.secretPlace);
exports.default = router;
//# sourceMappingURL=secret.js.map