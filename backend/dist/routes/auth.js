"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const auth_1 = require("../controllers/auth");
const validators_1 = require("../validators");
const auth_2 = require("../validators/auth");
const router = express_1.default.Router();
router.post("/signup", auth_2.userSignupValidator, validators_1.runValidation, auth_1.signup);
router.post("/signin", auth_2.userSigninValidator, validators_1.runValidation, auth_1.signin);
router.get("/signout", auth_1.signout);
exports.default = router;
//# sourceMappingURL=auth.js.map