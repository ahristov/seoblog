"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const system_1 = require("../controllers/system");
const router = express_1.default.Router();
router.get("/system/time", system_1.timeNow);
exports.default = router;
//# sourceMappingURL=system.js.map