"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const tag_1 = require("../controllers/tag");
const auth_1 = require("../middleware/auth");
const validators_1 = require("../validators");
const tag_2 = require("../validators/tag");
const router = express_1.default.Router();
router.post("/tag", auth_1.requireSignin, auth_1.authUserData, auth_1.adminUserOnly, tag_2.tagCreateValidator, validators_1.runValidation, tag_1.create);
router.delete("/tag/:slug", auth_1.requireSignin, auth_1.authUserData, auth_1.adminUserOnly, tag_1.remove);
router.get("/", tag_1.list);
router.get("/tag/:slug", tag_1.read);
exports.default = router;
//# sourceMappingURL=tag.js.map