"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
const express_jwt_1 = __importDefault(require("express-jwt"));
const user_1 = __importDefault(require("../models/user"));
// load env variables
dotenv_1.default.config();
const secret = process.env.JWT_SECRET;
const requireSignin = express_jwt_1.default({ secret });
exports.requireSignin = requireSignin;
const authUserData = (req, res, next) => {
    const authUserId = req.user._id;
    user_1.default.findById({ _id: authUserId }).exec((err, user) => {
        if (err || !user) {
            return res.status(400).json({
                error: "User not found"
            });
        }
        req.profile = user;
        next();
    });
};
exports.authUserData = authUserData;
const adminUserOnly = (req, res, next) => {
    const adminUserId = req.user._id;
    user_1.default.findById({ _id: adminUserId }).exec((err, user) => {
        if (err || !user) {
            return res.status(400).json({
                error: "User not found"
            });
        }
        if (user.role !== 1) {
            return res.status(400).json({
                error: "Admin resource. Access denied"
            });
        }
        req.profile = user;
        next();
    });
};
exports.adminUserOnly = adminUserOnly;
//# sourceMappingURL=auth.js.map