"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const body_parser_1 = __importDefault(require("body-parser"));
const cookie_parser_1 = __importDefault(require("cookie-parser"));
const cors_1 = __importDefault(require("cors"));
const dotenv_1 = __importDefault(require("dotenv"));
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const morgan_1 = __importDefault(require("morgan"));
const auth_1 = require("./middleware/auth");
const auth_2 = __importDefault(require("./routes/auth"));
const blog_1 = __importDefault(require("./routes/blog"));
const category_1 = __importDefault(require("./routes/category"));
const secret_1 = __importDefault(require("./routes/secret"));
const system_1 = __importDefault(require("./routes/system"));
const tag_1 = __importDefault(require("./routes/tag"));
const user_1 = __importDefault(require("./routes/user"));
// load env variables
dotenv_1.default.config();
// app
const app = express_1.default();
// db
mongoose_1.default
    .connect(process.env.MONGODB, {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => console.log("DB connected."))
    .catch((reason) => console.log("DB connect error", reason));
// middleware
app.use(morgan_1.default("dev"));
app.use(body_parser_1.default.json());
app.use(cookie_parser_1.default());
// cors
if (process.env.NODE_ENV === "development") {
    app.use(cors_1.default({ origin: `${process.env.CLIENT_URL}` }));
}
const secret = process.env.JWT_SECRET;
// routes middleware
app.use("/api/auth", auth_2.default);
app.use("/api/secret", auth_1.requireSignin, secret_1.default);
app.use("/api/user", auth_1.requireSignin, auth_1.authUserData, user_1.default);
app.use("/api/categories", category_1.default);
app.use("/api/tags", tag_1.default);
app.use("/api/blogs", blog_1.default);
app.use("/api/system", system_1.default);
// port
const port = process.env.PORT || 8000;
app.listen(port, () => {
    console.log(`Backend started at http://localhost:${port}/api`);
});
//# sourceMappingURL=index.js.map